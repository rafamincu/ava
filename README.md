# Avalara API

Yii2 component for Avalara API

## Add to project

```
$ composer require popglobal/avalara
```
or add in project file _composer.json_:

```
"require": {
    ...
    "popglobal/avalara": "dev-master"
}
"repositories": [
    ...
    {
        "type": "vcs",
        "url": "ssh://bitbucket.org/popglobal/avalara.git"
    }
]
```

## Configure logs module (backend)

Add to config file _backend/main.php_:
```
'modules' => [
    ...
    'avalara' => [
        'class' => 'popglobal\avalara\AvalaraModule',
        'controllerNamespace' => 'popglobal\avalara\controllers\backend',
    ],
],
```
Use backend URL: [_/admin_] _/avalara/log_ 

## Update database 

```
$ ./yii migrate/up --migrationPath=@vendor/popglobal/avalara/migrations
```

## Configure component

Add to config file _common/main-local.php_:

```
'components' => [
    ...
    'avalara' => [
        'class' => 'popglobal\avalara\components\Avalara',
        'endpoint' => ENDPOINT, // eg. https://communicationsua.avalara.net/api/v2/afc/
        'apiKey' => API_KEY, // from Avalara
    ],
],    
```

THE END
