<?php
namespace popglobal\avalara;

use yii\base\Module;

/**
 * AvalaraModule
 * @property bool $isBackend
 * @property string $imageFullPathUrl
 */
class AvalaraModule extends Module
{
    public $controllerNamespace = 'popglobal\avalara\controllers\backend';

    /**
     * Set paths
     */
    public function init()
    {
        parent::init();
        $this->setViewPath('@popglobal/avalara/views/backend');
    }
}
