<?php
namespace popglobal\avalara\controllers\console;

use yii;
use yii\console\Controller;

/**
 * Class TestController
 * @package popglobal\avalara\controllers\console
 */
class TestController extends Controller
{
    public $component = 'avalara';

    public function actionServiceInfo()
    {
        var_dump(Yii::$app->{$this->component}->serviceInfo());
    }

    public function actionCalcTaxes()
    {
        $document_id = 'D' . rand(1, 9999);
        $billing_address_data = ['ctry' => 'USA', 'st' => 'NC', 'city' => 'Durham', 'zip' => 27701];
        $items = [
            ['chg' => 200, 'sale' => 1, 'tran' => 19, 'serv' => 6],
        ];

        var_dump(Yii::$app->{$this->component}->calcTaxes($document_id, $billing_address_data, $items));
    }

    public function actionCommitStatus($document_id, $commit = false)
    {
        var_dump(Yii::$app->{$this->component}->commitStatus($document_id, $commit));
    }
}