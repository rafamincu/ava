<?php
namespace popglobal\avalara\components;

use yii;
use yii\base\Component;
use \Exception;
use popglobal\avalara\components\sdk\ApiRequest;
use popglobal\avalara\components\sdk\response\CalcTaxesResponse;

/** @noinspection PhpUndefinedClassInspection */

/**
 * Avalara API component
 * @author Doru Bratu <doru.bratu@gmail.com>
 */
class Avalara extends Component
{
    public $endpoint;
    public $apiKey;
    public $clientId;
    public $clientProfileId;
    public $timeout = 10;
    public $debug = 1;

    /** @var $request ApiRequest */
    public $request = null;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->request = new ApiRequest($this->endpoint, $this->apiKey, $this->clientId,
            $this->clientProfileId, $this->timeout, $this->debug);
    }

    /**
     * @return mixed
     */
    public function serviceInfo()
    {
        return json_decode($this->request->execute('serviceinfo', [], 'GET'));
    }

    /**
     * @param string $document_id
     * @param array $billing_address_data
     * @param array $items
     * @param bool $aggregate
     * @param string $date_time
     * @param array $company_data
     * @param int $customer_type
     * @return mixed
     * @throws Exception
     */
    public function calcTaxes($document_id, $billing_address_data, $items, $aggregate = false,
                              $currency = 'USD', $date_time = null, $company_data = [],
                              $customer_type = 1, $commit = false)
    {
        if (empty($date_time)) {
            $date_time = str_replace(' ', 'T', date('Y-m-d H:i:s'));
        }

        if (empty($company_data)) {
            $company_data = ['bscl' => 1, 'svcl' => 1, 'fclt' => true, 'frch' => true, 'reg' => true];
        }

        $response = $this->request->execute('CalcTaxes', [
            'cmpn' =>  $company_data,
            'inv' => [
                [
                    'doc' => $document_id,
                    'bill' => $billing_address_data,
                    'cust' => $customer_type,
                    'date' => $date_time,
                    'itms' => $items,
                    'cmmt' => $commit,
                    'ccycd' => $currency,
                ],
            ],
        ]);

        $data = json_decode($response, true);

        return $aggregate ? $this->aggregateTaxes($data) : new CalcTaxesResponse($data);
    }

    /**
     * @param string $document_id
     * @param bool $commit
     * @return mixed
     * @throws Exception
     */
    public function commitStatus($document_id, $commit = false)
    {
        $response = $this->request->execute('Commit', [
            'doc' =>  $document_id,
            'cmmt' => $commit,
        ]);

        $data = json_decode($response, true);

        return $data;
    }

    /**
     * @param $data
     * @return array
     * @throws \Throwable
     */
    protected function aggregateTaxes($data)
    {
        $taxes = [];
        foreach ($data['inv'] as $invoice_data) {
            if (!empty($invoice_data['err'])) {
                throw new Exception(implode('; ', $invoice_data['err'][0]));
            }

            foreach ($invoice_data['itms'] as $item_data) {
                if (!empty($item_data['err'])) {
                    throw new Exception(implode('; ', $item_data['err'][0]));
                }

                foreach ($item_data['txs'] as $item_tax_data) {
                    if (!empty($item_data['err'])) {
                        throw new Exception(implode('; ', $item_data['err'][0]));
                    }

                    if ($item_tax_data['bill'] == false) {
                        continue;
                    }

                    // remove Quebec Sales Tax (QST)
                    if ($item_tax_data['cat'] =='SALES AND USE TAXES' &&
                        $item_tax_data['name'] == 'Quebec Sales Tax (QST)') {
                        continue;
                    }

                    if (!isset($taxes[$item_tax_data['cid']])) {
                        $taxes[$item_tax_data['cid']] = [
                            'name' => $item_tax_data['cat'],
                            'types' => [], 'amount' => 0,
                        ];
                    }

                    if (!isset($taxes[$item_tax_data['cid']]['types'][$item_tax_data['tid']])) {
                        $taxes[$item_tax_data['cid']]['types'][$item_tax_data['tid']] = [
                            'name' => $item_tax_data['name'], 'amount' => 0,
                        ];
                    }

                    $taxes[$item_tax_data['cid']]['types'][$item_tax_data['tid']]['amount'] += $item_tax_data['tax'];
                    $taxes[$item_tax_data['cid']]['amount'] += $item_tax_data['tax'];
                }
            }
        }

        return $taxes;
    }
}