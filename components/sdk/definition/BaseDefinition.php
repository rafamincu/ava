<?php
namespace popglobal\avalara\components\sdk\definition;

/**
 * Class BaseDefinition
 * @package popglobal\avalara\components\sdk\definition
 */
class BaseDefinition
{
    public $err;

    public static $excludeProperties = [];

    public function __construct(array $data = [], $skipNonExisting = true)
    {
        $this->fromArray($data, $skipNonExisting);

        if (!empty($data['err'])) {
            $this->err = [];
            foreach ($data['err'] as $error_data) {
                $this->err[] = new Error($error_data);
            }
        }
    }

    public function toArray($nullAsEmpty = false)
    {
        $vars = get_object_vars($this);
        $data = array();
        foreach ($vars as $key => $value) {
            if ((!is_null($value)) || (is_null($value) && $nullAsEmpty)) {
                $data[$key] = $value;
            }
        }
        return $data;
    }

    public function fromArray($data, $skipNonExisting)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, self::$excludeProperties)) {
                continue;
            }
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
            else if (!$skipNonExisting) {
                throw new \Exception(get_class($this).' property ' . $key . ' does not exist !');
            }
        }
    }

    protected function clean()
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $value) {
            if ($value === null) {
                unset($this->{$key});
            }
        }
    }
}