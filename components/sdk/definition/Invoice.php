<?php
namespace popglobal\avalara\components\sdk\definition;

/**
 * Class Invoice
 * @package popglobal\avalara\components\sdk\definition
 */
class Invoice extends BaseDefinition
{
    public $doc;
    public $itms;

    public function __construct(array $invoice_data = [])
    {
        parent::__construct($invoice_data);

        if (!empty($invoice_data['itms'])) {
            $this->itms = [];
            foreach ($invoice_data['itms'] as $item_data) {
                $this->itms[] = new Item($item_data);
            }
        }
    }
}
