<?php

namespace popglobal\avalara\components\sdk;

use yii;
use \Exception;
use popglobal\avalara\models\AvalaraApiLog;

/**
 * Class ApiRequest for Avalara API
 *
 * @author Doru Bratu <doru.bratu@gmail.com>
 */
class ApiRequest {
    public $endpoint;
    public $apiKey;
    public $clientId;
    public $clientProfileId;
    public $requestTimeout;
    public $debugLevel;

    /**
     * ApiRequest constructor.
     *
     * @param string $endpoint
     * @param string $apiKey
     * @param string $clientId
     * @param string $clientProfileId
     * @param int    $requestTimeout
     * @param int    $debugLevel
     */
    public function __construct($endpoint, $apiKey, $clientId, $clientProfileId, $requestTimeout = 30, $debugLevel = 1)
    {
        $this->endpoint = $endpoint;
        $this->apiKey = $apiKey;
        $this->clientId = $clientId;
        $this->clientProfileId = $clientProfileId;
        $this->requestTimeout = $requestTimeout;
        $this->debugLevel = $debugLevel;
    }

    /**
     * executes API call
     *
     * @param string $urlPath
     * @param string $method
     * @param array  $requestData
     * @param bool   $auth
     * @param array  $extraHeaders
     * @param bool   $parse
     *
     * @return array|mixed
     * @throws Exception
     */
    public function execute(string $urlPath, array $data = [], $method = 'POST', array $extraHeaders = [])
    {
        if (!in_array(strtoupper($method), ['GET', 'POST'])) {
            throw new Exception('Invalid method :: ' . $method);
        }

        $url = $this->endpoint . $urlPath;
        $this->debug('Endpoint: ' . $url);

        foreach ($data as $key => $value) {
            if ($value === NULL) {
                unset($data[$key]);
            }
        }

        $headers = [
            'api_key: ' . $this->apiKey,
            'client_id: ' . $this->clientId,
            'client_profile_id: ' . $this->clientProfileId,
            'Content-Type: application/json',
            'Accept: application/json',
        ];

        $channel = curl_init($url);
        curl_setopt($channel, CURLOPT_CUSTOMREQUEST, $method);

        if (!empty($data)) {
            if (strtoupper($method == 'GET')) {
                $url .= '?' . http_build_query($data);
            } else {
                $data = json_encode($data);
                $headers[] = 'Content-Length: ' . strlen($data);
                curl_setopt($channel, CURLOPT_POSTFIELDS, $data);
            }
        }

        if (count($extraHeaders)) {
            $headers = array_merge($headers, $extraHeaders);
        }

        curl_setopt($channel, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($channel, CURLOPT_MAXREDIRS, 10);
        curl_setopt($channel, CURLOPT_ENCODING, "");
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($channel, CURLINFO_HEADER_OUT, true);
        curl_setopt($channel, CURLOPT_TIMEOUT, $this->requestTimeout);
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($channel);
        $channelInfo = curl_getinfo($channel);

        $apiKeyMask = str_replace(substr($this->apiKey, 1, -1), '***', $this->apiKey);
        $channelInfo['request_header'] = str_replace($this->apiKey, $apiKeyMask, $channelInfo['request_header']);

        if ($response === false) {
            $message = 'API Error: ' . curl_errno($channel) . ' - ' . curl_error($channel);
            $this->debug($message);
            AvalaraApiLog::add(['url' => $url, 'method' => $method, 'request_data' => $data, 'response_data' => $message, 'error' => true]);
            throw new Exception($message);
        }

        if (strpos($response, '504 Gateway Time-out') !== false) {
            $message = 'API Error: Gateway Time-out.';
            $this->debug($message);
            AvalaraApiLog::add(['url' => $url, 'method' => $method, 'request_data' => $data, 'response_data' => $message, 'error' => true]);
            throw new Exception($message);
        }

        if ($channelInfo['http_code'] != 200) {
            $this->debug($response);
            AvalaraApiLog::add(['url' => $url, 'method' => $method, 'request_data' => $data, 'response_data' => $response, 'error' => true]);
            throw new Exception('API Error: ' . $this->getErrorMessage($response));
        }

        $this->debug('Response: ' . $response, 2);
        AvalaraApiLog::add(['url' => $url, 'method' => $method, 'request_data' => $data, 'response_data' => $response, 'error' => false]);

        return $response;
    }

    /**
     * @param string $debugMessage
     * @param int    $level
     */
    protected function debug($debugMessage, $level = 1)
    {
        if ($level <= $this->debugLevel) {
            Yii::debug(date("r") . " " . (string)$debugMessage, 'AvalaraAPI');
        }
    }

    /**
     * @param string $response
     *
     * @return string
     */
    protected function getErrorMessage($response)
    {
        $message = $response;
        $result = json_decode($response, true);
        if (is_array($result)) {
            if (isset($result['error'])) {
                $message = (string)$result['error'];
            }
        }

        return (string)$message;
    }
}