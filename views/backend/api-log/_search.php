<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use popglobal\avalara\models\AvalaraApiLog;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model avalara\models\AvalaraApiLogSearch */
?>

<div class="avalara-log-search">
	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'url'); ?>
	<?= $form->field($model, 'method')->dropDownList(['' => 'All'] + AvalaraApiLog::$methods); ?>
	<?= $form->field($model, 'date_from')->widget(DatePicker::className(), ['dateFormat' => 'yyyy-MM-dd', 'options' => ['class' => 'form-control']]); ?>
	<?= $form->field($model, 'date_to')->widget(DatePicker::className(), ['dateFormat' => 'yyyy-MM-dd', 'options' => ['class' => 'form-control']]); ?>

	<div class="form-group buttons">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Reset', ['index'], ['class' => 'reset']); ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
